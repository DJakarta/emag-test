# eMAG Test

## Running the app locally
To run the app on localhost, run `npm run installAndBuildAndStart`.  
To optimize the start time, if you have already installed the packages via `npm run installAndBuildAndStart` or `npm install`, you can simply run `npm run buildAndStart`.

## Live version
A live version of the app is available at [emag-test.herokuapp.com](https://emag-test.herokuapp.com/).