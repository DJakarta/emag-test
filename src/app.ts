import express from "express";
import { fileURLToPath } from "url";
import { dirname } from "path";

const app = express();
const port = process.env.PORT || 80;
const __dirname = dirname(fileURLToPath(import.meta.url));

app.use(express.static(`${__dirname}/publicStatic`, {
  extensions: ["html"],
}));

app.listen(port, function () {
  console.log(`Listening on port ${port}.`);
});