import Combatant from "./Combatant.js";
import HeroStatSet from "./HeroStatSet.js";
import { MagicShieldSkill } from "./MagicShieldSkill.js";
import { RapidStrikeSkill } from "./RapidStrikeSkill.js";
import Skill from "./Skill.js";

export default class Hero implements Combatant {
  readonly stats: HeroStatSet;
  readonly skills: Skill[];
  readonly heroType: string;
  readonly randomNumberSeed?: number;
  readonly name: string;


  constructor(
    {
      randomNumberSeed,
      stats = HeroStatSet.fromRandomNumberSeed(randomNumberSeed),
      skills = [new RapidStrikeSkill({ randomNumberSeed }), new MagicShieldSkill({ randomNumberSeed })],
      heroType = "default hero type",
      name = "Default Hero"
    }: {
      stats?: HeroStatSet,
      skills?: Skill[],
      heroType?: string,
      randomNumberSeed?: number,
      name?: string
    } = {}
  ) {
    this.stats = stats;
    this.skills = skills;
    this.heroType = heroType;
    this.randomNumberSeed = randomNumberSeed;
    this.name = name;
  }

  is(combatant: Combatant): boolean {
    return (combatant instanceof Hero && combatant.heroType === this.heroType);
  }

  withName(name: string): Hero {
    return new Hero({ ...this, name });
  }

  withStats(stats: HeroStatSet): Hero {
    return new Hero({ ...this, stats });
  }

  withSkills(skills: Skill[]): Hero {
    return new Hero({ ...this, skills });
  }


  static fromRandomSeed(randomNumberSeed?: number): Hero {
    return new Hero({ randomNumberSeed });
  }
}