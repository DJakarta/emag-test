import Combatant from "./Combatant.js";
import TurnResult from "./TurnResult.js";

export default interface TurnRule {
  readonly attacker: Combatant;
  readonly defender: Combatant;


  withAttacker(attacker: Combatant): TurnRule;
  withDefender(defender: Combatant): TurnRule;
  withRandomNumberSeed(randomNumberSeed: number): TurnRule;
  getResult(): TurnResult;
}