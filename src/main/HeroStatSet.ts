import RangeBasedStatSet from "./RangeBasedStatSet.js";
import HeroStatRangeSet from "./HeroStatRangeSet.js";
import CombatantStatRangeSet from "./CombatantStatRangeSet.js";

export default class HeroStatSet extends RangeBasedStatSet {
  readonly intelligence: number;


  constructor(
    {
      health,
      strength,
      defence,
      speed,
      luck,
      ranges,
      randomNumberSeed,
      intelligence = 100
    }: {
      health: number;
      strength: number;
      defence: number;
      speed: number;
      luck: number;
      ranges?: HeroStatRangeSet;
      randomNumberSeed?: number;
      intelligence?: number;
    }
  ) {
    super({
      health,
      strength,
      defence,
      speed,
      luck,
      ranges,
      randomNumberSeed
    });
    this.intelligence = intelligence;
  }


  withHealth(health: number): HeroStatSet {
    return new HeroStatSet({ ...this, health });
  }

  withStrength(strength: number): HeroStatSet {
    return new HeroStatSet({ ...this, strength });
  }

  withDefence(defence: number): HeroStatSet {
    return new HeroStatSet({ ...this, defence });
  }

  withSpeed(speed: number): HeroStatSet {
    return new HeroStatSet({ ...this, speed });
  }

  withLuck(luck: number): HeroStatSet {
    return new HeroStatSet({ ...this, luck });
  }


  static fromRandomNumberSeed(randomNumberSeed?: number): HeroStatSet {
    return new HeroStatSet({ ...RangeBasedStatSet.fromRanges(new HeroStatRangeSet(), randomNumberSeed), randomNumberSeed });
  }
}