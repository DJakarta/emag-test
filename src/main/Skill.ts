import Combatant from "./Combatant.js";
import TurnResult from "./TurnResult.js";

export default class Skill {
  readonly name: string = "Generic Skill";

  
  applyTo(result: TurnResult, combatant: Combatant): TurnResult {
    return new TurnResult({
      ...result,
      appliedSkills: [...result.appliedSkills, {skill: this, effect: undefined}]
    });
  }
}