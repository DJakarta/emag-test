import Combatant from "./Combatant.js";
import BeastStatSet from "./BeastStatSet.js";
import Skill from "./Skill.js";

export default class Beast implements Combatant {
  readonly stats: BeastStatSet;
  readonly skills: Skill[];
  readonly beastType: string;
  readonly randomNumberSeed?: number;
  readonly name: string;


  constructor(
    {
      randomNumberSeed,
      stats = BeastStatSet.fromRandomNumberSeed(randomNumberSeed),
      skills = [],
      beastType = "default beast type",
      name = "Default Beast"
    }: {
      stats?: BeastStatSet,
      skills?: Skill[],
      beastType?: string,
      randomNumberSeed?: number,
      name?: string;
    } = {}
  ) {
    this.stats = stats;
    this.skills = skills;
    this.beastType = beastType;
    this.randomNumberSeed = randomNumberSeed;
    this.name = name;
  }


  is(combatant: Combatant): boolean {
    return (combatant instanceof Beast && combatant.beastType === this.beastType);
  }

  withName(name: string): Beast {
    return new Beast({ ...this, name });
  }

  withStats(stats: BeastStatSet): Beast {
    return new Beast({ ...this, stats });
  }

  withSkills(skills: Skill[]): Beast {
    return new Beast({ ...this, skills });
  }


  static fromRandomSeed(randomNumberSeed?: number): Beast {
    return new Beast().withStats(BeastStatSet.fromRandomNumberSeed(randomNumberSeed));
  }
}