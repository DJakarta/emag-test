import Combatant from "./Combatant.js";
import TurnResult from "./TurnResult.js";
import Random from "random-seed";
import SkillIgnoringTurn from "./SkillIgnoringTurn.js";

export default class SkillComputingTurn extends SkillIgnoringTurn {
  withAttacker(attacker: Combatant): SkillComputingTurn {
    return new SkillComputingTurn({ attacker, defender: this.defender, randomNumberSeed: this.randomNumberSeed });
  }

  withDefender(defender: Combatant): SkillComputingTurn {
    return new SkillComputingTurn({ attacker: this.attacker, defender, randomNumberSeed: this.randomNumberSeed });
  }

  withRandomNumberSeed(randomNumberSeed: number): SkillComputingTurn {
    return new SkillComputingTurn({ attacker: this.attacker, defender: this.defender, randomNumberSeed });
  }

  getResult(): TurnResult {
    const attackerSkillsResult: TurnResult = this.attacker.skills.reduce(
      (lastSkillResult, curentSkill) => (curentSkill.applyTo(lastSkillResult, this.attacker)),
      super.getResult()
    );

    const defenderSkillsResult: TurnResult = this.defender.skills.reduce(
      (lastSkillResult, curentSkill) => (curentSkill.applyTo(lastSkillResult, this.defender)),
      attackerSkillsResult
    );

    return defenderSkillsResult;
  };
}