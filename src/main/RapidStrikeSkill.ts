import Random from "random-seed";
import Combatant from "./Combatant.js";
import Skill from "./Skill.js";
import TurnResult from "./TurnResult.js";

export class RapidStrikeSkill extends Skill {
  readonly randomNumberSeed?: number;
  readonly chance: number;
  readonly name: string = "Rapid Strike";


  constructor(
    {
      randomNumberSeed,
      chance = 10
    }: {
      randomNumberSeed?: number,
      chance?: number
    } = {}
  ) {
    super();
    this.randomNumberSeed = randomNumberSeed;
    this.chance = chance;
  }


  applyTo(result: TurnResult, combatant: Combatant): TurnResult {
    if (result.attackerStartState.is(combatant)) {
      const randomGenerator = Random.create((this.randomNumberSeed !== undefined && this.randomNumberSeed !== null)
        ? String(this.randomNumberSeed)
        : undefined);
      const doubleStrikeLuck: boolean = randomGenerator.intBetween(0, 100) < this.chance;

      const damage = doubleStrikeLuck ? result.defenderStartState.stats.health - result.defenderEndState.stats.health : 0;

      return new TurnResult({
        ...result,
        defenderEndState: result.defenderEndState.withStats(result.defenderEndState.stats.withHealth(
          result.defenderEndState.stats.health - damage
        )),
        appliedSkills: doubleStrikeLuck ? [...result.appliedSkills, { skill: this, effect: undefined }] : result.appliedSkills
      });
    }
    else {
      return result;
    }
  }

  withRandomNumberSeed(randomNumberSeed: number): RapidStrikeSkill {
    return new RapidStrikeSkill({
      ...this,
      randomNumberSeed
    });
  }

  withChance(chance: number): RapidStrikeSkill {
    return new RapidStrikeSkill({
      ...this,
      chance
    });
  }
}