import Combatant from "./Combatant.js";
import Skill from "./Skill.js";
import TurnRule from "./TurnRule.js";

export default class TurnResult {
  readonly attackerStartState: Combatant;
  readonly defenderStartState: Combatant;
  readonly attackerEndState: Combatant;
  readonly defenderEndState: Combatant;
  readonly turnRule: TurnRule;
  readonly appliedSkills: {skill: Skill, effect: any}[];
  readonly effects: any[];

  
  constructor(
    {
      attackerStartState,
      defenderStartState,
      attackerEndState,
      defenderEndState,
      turnRule,
      appliedSkills = [],
      effects = []
    }: {
      attackerStartState: Combatant;
      defenderStartState: Combatant;
      attackerEndState: Combatant;
      defenderEndState: Combatant;
      turnRule: TurnRule;
      appliedSkills?: {skill: Skill, effect: any}[];
      effects?: any[];
    }
  ) {
    this.attackerStartState = attackerStartState;
    this.defenderStartState = defenderStartState;
    this.attackerEndState = attackerEndState;
    this.defenderEndState = defenderEndState;
    this.turnRule = turnRule;
    this.appliedSkills = appliedSkills;
    this.effects = effects;
  }
}