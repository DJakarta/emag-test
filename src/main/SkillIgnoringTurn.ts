import Combatant from "./Combatant.js";
import TurnRule from "./TurnRule.js";
import TurnResult from "./TurnResult.js";
import Random from "random-seed";

export default class SkillIgnoringTurn implements TurnRule {
  readonly attacker: Combatant;
  readonly defender: Combatant;
  readonly randomNumberSeed?: number;


  constructor(
    {
      attacker,
      defender,
      randomNumberSeed
    }: {
      attacker: Combatant;
      defender: Combatant;
      randomNumberSeed?: number;
    }
  ) {
    this.attacker = attacker;
    this.defender = defender;
    this.randomNumberSeed = randomNumberSeed;
  }


  withAttacker(attacker: Combatant): SkillIgnoringTurn {
    return new SkillIgnoringTurn({ attacker, defender: this.defender, randomNumberSeed: this.randomNumberSeed });
  }

  withDefender(defender: Combatant): SkillIgnoringTurn {
    return new SkillIgnoringTurn({ attacker: this.attacker, defender, randomNumberSeed: this.randomNumberSeed });
  }

  withRandomNumberSeed(randomNumberSeed: number): SkillIgnoringTurn {
    return new SkillIgnoringTurn({ attacker: this.attacker, defender: this.defender, randomNumberSeed });
  }

  getResult(): TurnResult {
    const randomGenerator = Random.create((this.randomNumberSeed !== undefined && this.randomNumberSeed !== null)
      ? String(this.randomNumberSeed)
      : undefined);
    const noDamageLuck: boolean = randomGenerator.intBetween(0, 100) < this.defender.stats.luck;

    const damage = noDamageLuck ? 0 : this.attacker.stats.strength - this.defender.stats.defence;

    const intermediateTurnResult = new TurnResult({
      attackerStartState: this.attacker,
      defenderStartState: this.defender,
      attackerEndState: this.attacker,
      defenderEndState: this.defender.withStats(this.defender.stats.withHealth(this.defender.stats.health - damage)),
      turnRule: this,
      effects: noDamageLuck ? ["noDamageLuck"] : []
    });

    return intermediateTurnResult;
  };
}