import CombatantStatSet from "./CombatantStatSet.js";
import Skill from "./Skill.js";

export default interface Combatant {
  readonly stats: CombatantStatSet;
  readonly skills: Skill[];
  readonly name: string;

  is(combatant: Combatant): boolean;
  withStats(stats: CombatantStatSet): Combatant;
  withSkills(skills: Skill[]): Combatant;
  withName(name: string): Combatant;
}