import CombatantStatRangeSet from "./CombatantStatRangeSet";
import StatRange from "./StatRange";

export default class BeastStatRangeSet implements CombatantStatRangeSet {
  readonly healthRange: StatRange = [60, 90];
  readonly strengthRange: StatRange = [60, 90];
  readonly defenceRange: StatRange = [40, 60];
  readonly speedRange: StatRange = [40, 60];
  readonly luckRange: StatRange = [25, 40];
}