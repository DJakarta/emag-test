import Random from "random-seed";
import CombatantStatRangeSet from "./CombatantStatRangeSet.js";
import DefaultCombatantStatSet from "./DefaultCombatantStatSet.js";

export default class RangedBasedStatSet extends DefaultCombatantStatSet {
  readonly ranges?: CombatantStatRangeSet;
  readonly randomNumberSeed?: number;


  constructor(
    {
      health,
      strength,
      defence,
      speed,
      luck,
      ranges,
      randomNumberSeed
    }: {
      health: number;
      strength: number;
      defence: number;
      speed: number;
      luck: number;
      ranges?: CombatantStatRangeSet;
      randomNumberSeed?: number;
    }
  ) {
    super({
      health,
      strength,
      defence,
      speed,
      luck
    });
    this.ranges = ranges;
    this.randomNumberSeed = randomNumberSeed;
  }


  static fromRanges(ranges: CombatantStatRangeSet, randomNumberSeed?: number): RangedBasedStatSet {
    const randomGenerator = Random.create((randomNumberSeed !== undefined && randomNumberSeed !== null)
      ? String(randomNumberSeed)
      : undefined);
    return new RangedBasedStatSet({
      health: randomGenerator.intBetween(ranges.healthRange[0], ranges.healthRange[1]),
      strength: randomGenerator.intBetween(ranges.strengthRange[0], ranges.strengthRange[1]),
      defence: randomGenerator.intBetween(ranges.defenceRange[0], ranges.defenceRange[1]),
      speed: randomGenerator.intBetween(ranges.speedRange[0], ranges.speedRange[1]),
      luck: randomGenerator.intBetween(ranges.luckRange[0], ranges.luckRange[1]),
      ranges,
      randomNumberSeed
    });
  }
}