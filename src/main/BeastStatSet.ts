import RangeBasedStatSet from "./RangeBasedStatSet.js";
import BeastStatRangeSet from "./BeastStatRangeSet.js";

export default class BeastStatSet extends RangeBasedStatSet {
  readonly viciousness: number;


  constructor(
    {
      health,
      strength,
      defence,
      speed,
      luck,
      ranges,
      randomNumberSeed,
      viciousness = 100
    }: {
      health: number;
      strength: number;
      defence: number;
      speed: number;
      luck: number;
      ranges?: BeastStatRangeSet;
      randomNumberSeed?: number;
      viciousness?: number;
    }
  ) {
    super({
      health,
      strength,
      defence,
      speed,
      luck,
      ranges,
      randomNumberSeed
    });
    this.viciousness = viciousness;
  }


  withHealth(health: number): BeastStatSet {
    return new BeastStatSet({ ...this, health });
  }

  withStrength(strength: number): BeastStatSet {
    return new BeastStatSet({ ...this, strength });
  }

  withDefence(defence: number): BeastStatSet {
    return new BeastStatSet({ ...this, defence });
  }

  withSpeed(speed: number): BeastStatSet {
    return new BeastStatSet({ ...this, speed });
  }

  withLuck(luck: number): BeastStatSet {
    return new BeastStatSet({ ...this, luck });
  }


  static fromRandomNumberSeed(randomNumberSeed?: number): BeastStatSet {
    return new BeastStatSet({ ...RangeBasedStatSet.fromRanges(new BeastStatRangeSet(), randomNumberSeed), randomNumberSeed });
  }
}