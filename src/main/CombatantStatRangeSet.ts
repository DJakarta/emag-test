import StatRange from "./StatRange.js";

export interface HealthStatRange {
  readonly healthRange: StatRange;
}

export interface StrengthStatRange {
  readonly strengthRange: StatRange;
}

export interface DefenceStatRange {
  readonly defenceRange: StatRange;
}

export interface SpeedStatRange {
  readonly speedRange: StatRange;
}

export interface LuckStatRange {
  readonly luckRange: StatRange;
}

export default interface CombatantStatRangeSet extends
  HealthStatRange,
  StrengthStatRange,
  DefenceStatRange,
  SpeedStatRange,
  LuckStatRange {
}