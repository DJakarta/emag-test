import Combatant from "./Combatant.js";
import Skill from "./Skill.js";
import TurnResult from "./TurnResult.js";

export default class DamageSkill extends Skill {
  applyTo(result: TurnResult, combatant: Combatant): TurnResult {
    const temporaryResult: TurnResult = new TurnResult({
      attackerStartState: result.attackerStartState,
      defenderStartState: result.defenderStartState,
      attackerEndState: result.attackerEndState,
      defenderEndState: result.defenderEndState,
      turnRule: result.turnRule
    });
    return super.applyTo(temporaryResult, combatant);
  }
}