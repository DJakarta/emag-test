export interface HealthStat {
  readonly health: number;
  withHealth(health: number): HealthStat;
}

export interface StrengthStat {
  readonly strength: number;
  withStrength(strength: number): StrengthStat;
}

export interface DefenceStat {
  readonly defence: number;
  withDefence(defence: number): DefenceStat;
}

export interface SpeedStat {
  readonly speed: number;
  withSpeed(speed: number): SpeedStat;
}

export interface LuckStat {
  readonly luck: number;
  withLuck(luck: number): SpeedStat;
}

export default interface CombatantStatSet extends
  HealthStat,
  StrengthStat,
  DefenceStat,
  SpeedStat,
  LuckStat {
  withHealth(health: number): CombatantStatSet;
  withStrength(strength: number): CombatantStatSet;
  withDefence(defence: number): CombatantStatSet;
  withSpeed(speed: number): CombatantStatSet;
  withLuck(luck: number): CombatantStatSet;
}