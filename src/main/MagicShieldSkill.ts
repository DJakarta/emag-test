import Random from "random-seed";
import Combatant from "./Combatant.js";
import Skill from "./Skill.js";
import TurnResult from "./TurnResult.js";

export class MagicShieldSkill extends Skill {
  readonly randomNumberSeed?: number;
  readonly chance: number;
  readonly name: string = "Magic Shield";


  constructor(
    {
      randomNumberSeed,
      chance = 20
    }: {
      randomNumberSeed?: number,
      chance?: number
    } = {}
  ) {
    super();
    this.randomNumberSeed = randomNumberSeed;
    this.chance = chance;
  }


  applyTo(result: TurnResult, combatant: Combatant): TurnResult {
    if (result.defenderStartState.is(combatant)) {
      const randomGenerator = Random.create((this.randomNumberSeed !== undefined && this.randomNumberSeed !== null)
        ? String(this.randomNumberSeed)
        : undefined);
      const halfDamageLuck: boolean = randomGenerator.intBetween(0, 100) < this.chance;

      const removedDamage = halfDamageLuck ? (result.defenderStartState.stats.health - result.defenderEndState.stats.health) / 2 : 0;

      return new TurnResult({
        ...result,
        defenderEndState: result.defenderEndState.withStats(result.defenderEndState.stats.withHealth(
          result.defenderEndState.stats.health + removedDamage
        )),
        appliedSkills: halfDamageLuck ? [...result.appliedSkills, { skill: this, effect: undefined }] : result.appliedSkills
      });
    }
    else {
      return result;
    }
  }

  withRandomNumberSeed(randomNumberSeed: number): MagicShieldSkill {
    return new MagicShieldSkill({
      ...this,
      randomNumberSeed
    });
  }

  withChance(chance: number): MagicShieldSkill {
    return new MagicShieldSkill({
      ...this,
      chance
    });
  }
}