import CombatantStatRangeSet from "./CombatantStatRangeSet";
import StatRange from "./StatRange";

export default class HeroStatRangeSet implements CombatantStatRangeSet {
  readonly healthRange: StatRange = [70, 100];
  readonly strengthRange: StatRange = [70, 80];
  readonly defenceRange: StatRange = [45, 55];
  readonly speedRange: StatRange = [40, 50];
  readonly luckRange: StatRange = [10, 30];
}