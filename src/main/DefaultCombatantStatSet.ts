import CombatantStatSet from "./CombatantStatSet";

export default class DefaultCombatantStatSet implements CombatantStatSet {
  readonly health: number;
  readonly strength: number;
  readonly defence: number;
  readonly speed: number;
  readonly luck: number;


  constructor(
    {
      health,
      strength,
      defence,
      speed,
      luck
    }: {
      health: number;
      strength: number;
      defence: number;
      speed: number;
      luck: number;
    }
  ) {
    this.health = health;
    this.strength = strength;
    this.defence = defence;
    this.speed = speed;
    this.luck = luck;
  }


  withHealth(health: number): DefaultCombatantStatSet {
    return new DefaultCombatantStatSet({ ...this, health: health });
  }
  withStrength(strength: number): DefaultCombatantStatSet {
    return new DefaultCombatantStatSet({ ...this, strength: strength });
  }
  withDefence(defence: number): DefaultCombatantStatSet {
    return new DefaultCombatantStatSet({ ...this, defence: defence });
  }
  withSpeed(speed: number): DefaultCombatantStatSet {
    return new DefaultCombatantStatSet({ ...this, speed: speed });
  }
  withLuck(luck: number): DefaultCombatantStatSet {
    return new DefaultCombatantStatSet({ ...this, luck: luck });
  }
}