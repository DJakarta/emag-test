import Combatant from "./Combatant.js";
import SkillComputingTurn from "./SkillComputingTurn.js";
import SkillIgnoringTurn from "./SkillIgnoringTurn.js";
import TurnResult from "./TurnResult.js";
import TurnRule from "./TurnRule.js";

export default class Battle {
  private readonly maximumTurns: number = 20;
  readonly turnResults: TurnResult[];
  readonly combatant1: Combatant;
  readonly combatant2: Combatant;
  readonly turnRule: TurnRule;
  readonly randomNumberSeed?: number;


  constructor(
    {
      combatant1,
      combatant2,
      turns,
      randomNumberSeed
    }: {
      combatant1: Combatant;
      combatant2: Combatant;
      turns?: TurnResult[];
      randomNumberSeed?: number;
    }
  ) {
    this.combatant1 = combatant1;
    this.combatant2 = combatant2;
    this.turnResults = turns ? turns : [];
    this.turnRule = new SkillComputingTurn({
      attacker: combatant1,
      defender: combatant2,
      randomNumberSeed
    });
    this.randomNumberSeed = randomNumberSeed;
  }


  isFinished(): boolean {
    return (this.turnResults.length >= this.maximumTurns
      || (!!this.turnResults.length && (
        this.turnResults[this.turnResults.length - 1].defenderEndState.stats.health <= 0
        || this.turnResults[this.turnResults.length - 1].attackerEndState.stats.health <= 0
      ))
    );
  }

  fightTurn(turnRule?: TurnRule): Battle {
    if (!this.isFinished()) {
      const lastTurnResult = this.turnResults.length
        ? this.turnResults[this.turnResults.length - 1]
        : undefined;
      const currentAttacker: Combatant = lastTurnResult
        ? lastTurnResult.defenderEndState
        : Battle.firstAttacker(this.combatant1, this.combatant2);
      const currentDefender: Combatant = lastTurnResult
        ? lastTurnResult.attackerEndState
        : Battle.firstDefender(this.combatant1, this.combatant2);
      const currentTurnRule = (turnRule ?? this.turnRule).withAttacker(currentAttacker).withDefender(currentDefender);
      const nextTurnRule = (this.randomNumberSeed !== undefined && this.randomNumberSeed !== null)
        ? currentTurnRule.withRandomNumberSeed(this.randomNumberSeed + this.turnResults.length)
        : currentTurnRule;

      const turnResult: TurnResult = nextTurnRule.getResult();

      return new Battle({
        ...this,
        turns: [...this.turnResults, turnResult]
      });
    }
    else {
      throw new Error("Could not generate next turn because the battle has finished.");
    }
  }

  finish(): Battle {
    if (!this.isFinished()) {
      return this.fightTurn().finish();
    }
    else {
      return this;
    }
  }

  loser(): Combatant | null {
    if (!!this.turnResults.length) {
      const lastTurnResult = this.turnResults[this.turnResults.length - 1];
      if (
        lastTurnResult.defenderEndState.stats.health <= 0
      ) {
        return lastTurnResult.defenderEndState;
      }
      else if (lastTurnResult.attackerEndState.stats.health <= 0) {
        return lastTurnResult.attackerEndState;
      }
      else {
        return null;
      }
    }
    else {
      return null;
    }
  }

  winner(): Combatant | null {
    const loser = this.loser();
    if (loser) {
      if (loser.is(this.combatant1)) {
        return this.combatant2;
      }
      else {
        return this.combatant1;
      }
    }
    else {
      return null;
    }
  }


  static firstAttacker(combatant1: Combatant, combatant2: Combatant): Combatant {
    if (combatant1.stats.speed > combatant2.stats.speed) {
      return combatant1;
    }
    else if (combatant2.stats.speed > combatant1.stats.speed) {
      return combatant2;
    }
    else {
      if (combatant1.stats.luck >= combatant2.stats.luck) {
        return combatant1;
      }
      else {
        return combatant2;
      }
    }
  }

  static firstDefender(combatant1: Combatant, combatant2: Combatant): Combatant {
    if (Battle.firstAttacker(combatant1, combatant2) == combatant1) {
      return combatant2;
    }
    else {
      return combatant1;
    }
  }
}