import { assert } from "chai";
import Hero from "../main/Hero.js";
import checkCombatantStat from "./checkCombatantStat.js";
import TestsConfig from "./TestsConfig.js"

export function checkHeroStatsRange(hero: Hero) {
  checkCombatantStat(hero, "health", { range: [70, 100] });
  checkCombatantStat(hero, "strength", { range: [70, 80] });
  checkCombatantStat(hero, "defence", { range: [45, 55] });
  checkCombatantStat(hero, "speed", { range: [40, 50] });
  checkCombatantStat(hero, "luck", { range: [10, 30] });
}

export function checkHeroStatsValues(hero: Hero, value: number) {
  checkCombatantStat(hero, "health", { value });
  checkCombatantStat(hero, "strength", { value });
  checkCombatantStat(hero, "defence", { value });
  checkCombatantStat(hero, "speed", { value });
  checkCombatantStat(hero, "luck", { value });
}

let heroes: Hero[] = [...new Array<Hero>(TestsConfig.iterations)].map(
  (hero, index) => Hero.fromRandomSeed(index)
);

describe("Hero test.", () => {
  it("Hero stats.", () => {
    heroes.forEach(checkHeroStatsRange);
  });
  it("Hero with stat.", () => {
    heroes.map(
      (hero, index) => (
        hero.withStats(hero.stats.withHealth(index).withStrength(index).withDefence(index).withSpeed(index).withLuck(index))
      )
    ).forEach((hero, index) => checkHeroStatsValues);
  });
});