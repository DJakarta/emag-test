import chai from "chai";
import Combatant from "../main/Combatant.js";
import StatRange from "../main/StatRange.js";
const assert = chai.assert;

export default function checkCombatantStat(
  combatant: Combatant,
  statName: string,
  {
    range,
    value
  }: {
    range?: StatRange,
    value?: number
  }
) {
  assert.isTrue(
    Number.isInteger((combatant.stats as any)[statName]),
    `Stat ${statName} should be integer.`
  );
  if (range) {
    assert.isAtLeast(
      (combatant.stats as any)[statName],
      range[0],
      `Stat ${statName} should be more than the minimum.`
    );
    assert.isAtMost(
      (combatant.stats as any)[statName],
      range[1],
      `Stat ${statName} should be less than the maximum.`
    );
  }
  !value ?? assert.strictEqual(
    (combatant.stats as any)[statName],
    value,
    `Stat ${statName} should be less than the maximum.`
  );
}
