import Beast from "../main/Beast.js";
import Combatant from "../main/Combatant.js";
import checkCombatantStat from "./checkCombatantStat.js";
import TestsConfig from "./TestsConfig.js";

export function checkBeastStats(beast: Beast) {
  checkCombatantStat(beast, "health", { range: [60, 90] });
  checkCombatantStat(beast, "strength", { range: [60, 90] });
  checkCombatantStat(beast, "defence", { range: [40, 60] });
  checkCombatantStat(beast, "speed", { range: [40, 60] });
  checkCombatantStat(beast, "luck", { range: [25, 40] });
}

export function checkBeastStatsValues(beast: Beast, value: number) {
  checkCombatantStat(beast, "health", { value });
  checkCombatantStat(beast, "strength", { value });
  checkCombatantStat(beast, "defence", { value });
  checkCombatantStat(beast, "speed", { value });
  checkCombatantStat(beast, "luck", { value });
}

let beasts: Beast[] = [...new Array<Beast>(TestsConfig.iterations)].map(
  (hero, index) => Beast.fromRandomSeed(index)
);

describe("Beast test.", () => {
  it("Beast stats.", () => {
    beasts.forEach(checkBeastStats);
  });
  it("Hero with stat.", () => {
    beasts.map(
      (beast, index) => (
        beast.withStats(beast.stats.withHealth(index).withStrength(index).withDefence(index).withSpeed(index).withLuck(index))
      )
    ).forEach((beast, index) => checkBeastStatsValues);
  });
});