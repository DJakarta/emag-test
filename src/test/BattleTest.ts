import chai from "chai";
import Battle from "../main/Battle.js";
import Beast from "../main/Beast.js";
import BeastStatSet from "../main/BeastStatSet.js";
import Hero from "../main/Hero.js";
import HeroStatSet from "../main/HeroStatSet.js";
import TurnResult from "../main/TurnResult.js";
import TestsConfig from "./TestsConfig.js";
const assert = chai.assert;

function createBattles(): Battle[] {
  return [...new Array<Battle>(TestsConfig.iterations)].map(
    (battle, index) => (
      new Battle({
        combatant1: Hero.fromRandomSeed(index),
        combatant2: Beast.fromRandomSeed(index),
        randomNumberSeed: index
      }).finish()
    )
  );
}

const battles: Battle[] = createBattles();

describe("Battle test.", () => {
  it("Maximum number of rounds.", () => {
    battles.forEach((battle) => {
      assert.isAtMost(battle.turnResults.length, 20, "Maximum number of rounds must be less than 20.");
    });
  });

  it("First turn.", () => {
    battles.forEach((battle, i) => {
      const message = "The first attacker should be the one with the higher speed, or if the speeds are equal, the one with the higher luck.";
      const firstTurnResult = battle.turnResults[0];
      const firstTurnAttacker = firstTurnResult.attackerStartState;
      const firstTurnDefender = firstTurnResult.defenderStartState;
      const combatant1 = battle.combatant1;
      const combatant2 = battle.combatant2;
      const combatant1Class = combatant1.constructor;
      const combatant2Class = combatant2.constructor;

      if (combatant1.stats.speed > combatant2.stats.speed) {
        assert.instanceOf(firstTurnAttacker, combatant1Class, message);
        assert.instanceOf(firstTurnDefender, combatant2Class, message);
      }
      else if (combatant1.stats.speed < combatant2.stats.speed) {
        assert.instanceOf(firstTurnAttacker, combatant2Class, message);
        assert.instanceOf(firstTurnDefender, combatant1Class, message);
      }
      else {
        if (combatant1.stats.luck > combatant2.stats.luck) {
          assert.instanceOf(firstTurnAttacker, combatant1Class, message);
          assert.instanceOf(firstTurnDefender, combatant2Class, message);
        }
        else {
          assert.instanceOf(firstTurnAttacker, combatant2Class, message);
          assert.instanceOf(firstTurnDefender, combatant1Class, message);
        }
      }
    });
  });

  it("Turn change.", () => {
    battles.forEach((battle) => {
      battle.turnResults.forEach((result, index, array) => {
        if (index > 0) {
          const lastResult: TurnResult = array[index - 1];
          assert.instanceOf(
            result.attackerStartState,
            lastResult.defenderStartState.constructor,
            "The current turn's attacker should be the last round's defender."
          );
          assert.instanceOf(
            result.defenderStartState,
            lastResult.attackerStartState.constructor,
            "The current turn's defender should be the last round's attacker."
          );
        }
      });
    });
  });

  it("Deterministic battle.", () => {
    const battles2 = createBattles();
    assert.deepEqual(battles2, battles, "The course of the battles should be the same when given the same random number seeds.");
  })
});

describe("Battle turn test.", () => {
  it("Turn test.", () => {
    const hero = new Hero({ stats: HeroStatSet.fromRandomNumberSeed().withHealth(100) });
    const beast = new Beast({ stats: BeastStatSet.fromRandomNumberSeed().withHealth(50) });
  });
})