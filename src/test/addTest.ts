import chai from "chai";
const assert = chai.assert;
import add from "../main/add.js";

describe("Add test.", () => {
  it("Integer addition.", () => {
    assert.strictEqual(add(2, 3), 5, "2 plus 3 should be equal to 5.");
  });
});