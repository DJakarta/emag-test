import Battle from "../main/Battle.js";
import Beast from "../main/Beast.js";
import Hero from "../main/Hero.js";

// @ts-ignore
const createApp = Vue.createApp;

const App = {
  data(): {
    seedSource: string,
    randomNumberSeedInput: number,
    randomNumberSeed: number,
    battle?: Battle
  } {
    return {
      seedSource: "random",
      randomNumberSeedInput: 0,
      randomNumberSeed: 0,
      battle: new Battle({
        combatant1: Hero.fromRandomSeed(0),
        combatant2: Beast.fromRandomSeed(0),
        randomNumberSeed: 0
      }).finish()
    }
  },
  methods: {
    generateBattle() {
      // @ts-ignore
      const battleSeed = this.seedSource === "random"
        // @ts-ignore  
        ? this.randomNumberSeedInput = Math.floor(Math.random() * 1000000)
        // @ts-ignore
        : this.randomNumberSeedInput;
      // @ts-ignore
      this.randomNumberSeed = battleSeed;

      // @ts-ignore
      this.battle = new Battle({
        combatant1: Hero.fromRandomSeed(battleSeed),
        combatant2: Beast.fromRandomSeed(battleSeed),
        randomNumberSeed: battleSeed
      }).finish();
    }
  }
}
const vueApp = createApp(App).mount("#app");